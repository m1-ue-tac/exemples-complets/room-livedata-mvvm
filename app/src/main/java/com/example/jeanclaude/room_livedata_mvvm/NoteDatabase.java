package com.example.jeanclaude.room_livedata_mvvm;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

// définition de la structure de la BDD avec la liste des entités manipulées*
// et le n° de version de la BDD
@Database(entities = {Note.class}, version = 1)
public abstract class NoteDatabase extends RoomDatabase {

    // pour créer un Singleton de la BDD
    private static NoteDatabase instance;

    public abstract NoteDao noteDao();

    // création du singleton de la BDD
    // - synchronized garantit qu'une seule instance sera créée même si plusieurs threads essaient de le faire en parallèle
    // - fallbackToDestructiveMigration permet de créer/recréer la base si ce n'est pas la bonne version
    public static synchronized NoteDatabase getInstance(Context context){
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),NoteDatabase.class,"note_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;

    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void,Void,Void>{
        private NoteDao noteDao;

        public PopulateDbAsyncTask(NoteDatabase db) {
            noteDao = db.noteDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.insert(new Note("Note 1", "Description 1", 1));
            noteDao.insert(new Note("Note 2", "Description 2", 2));
            noteDao.insert(new Note("Note 3", "Description 3", 3));
            return null;
        }
    }
}
